# wmpoly

A polygon/polyhedron intersection/containment module for WorldMaster.  This should allow getting boundaries, moving shapes, polygon prism shapes, intersection, containment, and various other concerns using integer coordinates.